import { Component, NgModule, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidationdirectiveDirective } from './validationdirective.directive';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SocialUser, SocialAuthService, GoogleSigninButtonModule } from '@abacritt/angularx-social-login';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})


export class LoginComponent implements OnInit {

  user: SocialUser;
  loggedIn: boolean = false;
  email: string = "";
  password: string = "";

  constructor(
    private _router: Router,
    private _spinner: NgxSpinnerService,
    private authService: SocialAuthService,
    private _authServiceLogin: AuthService,
    private _toastr: ToastrService,
  ) { }

  ngOnInit() {

    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log(this.user);
      if (this.loggedIn) {
        this.loginGoogle();
      }
    });

    this._authServiceLogin.loggedIn$.subscribe((status) => {
      this.loggedIn = status;
      if (this.loggedIn) {
        //this._router.navigate([""]);
        this._router.navigateByUrl("/");
      }
    });
  }

  isLoading = false;

  login() {

    if (this.email != 'ccet' && this.password != '123') {
      this._toastr.error("Kullanıcı Adı veya Şifre Hatalı", "Error");
    } else {
      this.isLoading = true;
      this._spinner.show();

      setTimeout(() => {
        try {
          this._authServiceLogin.login();
        } catch (error) {
          console.error("Hata:", error);
        } finally {
          this.isLoading = false;
          this._spinner.hide();
        }
      }, 2000);

    };
  }


  loginGoogle() {

    this.isLoading = true;
    this._spinner.show();

    setTimeout(() => {
      try {
        this._authServiceLogin.login();
      } catch (error) {
        console.error("Hata:", error);
      } finally {
        this.isLoading = false;
        this._spinner.hide();
      }
    }, 2000);

  }
}


@NgModule({
  declarations: [
    LoginComponent,
    ValidationdirectiveDirective
  ],
  imports: [
    FormsModule, // FormsModule'ü import et
    CommonModule, // CommonModule'ü import et
    NgxSpinnerModule,
    BrowserAnimationsModule,
    GoogleSigninButtonModule
  ],
  exports: [
    LoginComponent
  ]
})


export class LoginModule { }
