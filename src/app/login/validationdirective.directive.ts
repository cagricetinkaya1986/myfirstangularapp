import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appValidationdirective]'
})


export class ValidationdirectiveDirective {

  constructor(
    private el: ElementRef
  ) { }

  @HostListener('keyup') onKeyUp() {
    this.checkInputValidate(this.el.nativeElement);
  }

  checkInputValidate(element: any) {
    const valid = element.validity.valid;
    const id = element.id;
    const divEl = document.querySelector(`#${id} + div`);
    if (valid) {
      element.className = "valid";
      divEl!.innerHTML = "";
    } else {
      element.className = "invalid";
      divEl!.innerHTML = element.validationMessage;
    }
  }

}
