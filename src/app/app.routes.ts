import { Routes, RouterModule, Route } from '@angular/router';
import { Test2Component } from './test2/test2.component';
import { Test3Component } from './test3/test3.component';
import { TestComponent } from './test/test.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { TodoComponent } from './todo/todo.component';
import { CreditComponent } from './credit/credit.component';
import { Servicetest1Component } from './servicetest1/servicetest1.component';
import { authGuard } from './auth.guard';
import { authchildGuard } from './authchild.guard';
import { inject } from '@angular/core';
import { AuthService } from './auth.service';
import { exitGuard } from './exit.guard';
import { WebapiComponent } from './webapi/webapi.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { NgFormComponent } from './ng-form/ng-form.component';
import { LazyloadingComponent } from './lazyloading/lazyloading.component';
import { Test4component } from './test4/test4.component';


// test2 ye hem parametreli hem parametresiz ulasılabilir bu parametreli
const test2Route: Route = {
  path: 'test2/:params',
  component: Test2Component
}

export const routes: Routes = [
  {
    path: 'login', component: LoginComponent
  },
  {
    path: '', component: LayoutComponent,
    // canActivateChild guard ile kullanımı
    // canActivateChild:[authchildGuard],
    // canActivateChild servis ile kullanımı
    canActivateChild: [() => inject(AuthService).isAuthenticated(), authchildGuard],
    children: [
      {
        path: '',
        // canActivate component için canActivateChild ise menuler için
        //canActivate:[authGuard],
        canDeactivate: [exitGuard],
        component: TestComponent
      },
      { path: 'test', canDeactivate: [exitGuard], /*canActivate:[authGuard],*/ component: TestComponent },
      // test2 ye hem parametreli hem parametresiz ulasılabilir bu parametresiz
      { path: 'test2', component: Test2Component },
      test2Route,
      { path: 'test3', component: Test3Component },
      { path: 'test4',  loadComponent: () => import('../app/test4/test4.component').then(m => m.Test4component), },
      {
        path: 'todo', component: TodoComponent
      },
      {
        path: 'credit', component: CreditComponent

      },
      {
        path: 'service', component: Servicetest1Component
      },
      {
        path: 'webapi', component: WebapiComponent
      },
      {
        path: 'reactive', component: ReactiveFormComponent
      },
      {
        path: 'ngform', component: NgFormComponent
      },
      {
        path: 'lazyloading',
        loadComponent: () => import('../app/lazyloading/lazyloading.component').then(m => m.LazyloadingComponent),
      },
      {
        path: '**',
        component: NotFoundComponent,
      }
    ]
  }
];
