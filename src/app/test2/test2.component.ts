import { Component, ViewChild, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-test2',
  standalone: true,
  imports: [CommonModule,FormsModule],
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.scss'
})

export class Test2Component {


  //Adres Çubugundan Gelen Parametreyi Yakalama
  constructor(
    private activated: ActivatedRoute
  ) {

    this.activated.params.subscribe((res) => {
      console.log("params:" + res['params']);
    })
  }

  kutucuks: string[] = ["", "", "", "", "", "", "", "", ""]
  index: any = 0;
  message: string = "Dinamik Kutucuk Hiç Kutucuk Tıklanmadı ";
  selectedButtonIndex: number = -1;


  getValue() {
    let element: any = document.getElementById("name");
    console.log("test2app:" + element.value);
  }

  getValueTwo(event: any) {
    console.log(event.target.value);
  }

  @ViewChild("mail") test!: ElementRef<HTMLInputElement>; // ! işareti kullanarak TypeScript'e bu değişkenin ileride mutlaka atanacağını belirtiyoruz

  getValueThreee() {
    console.log(this.test.nativeElement.value);
  }

  setMark(index: any) {
    this.message = index + " Numaralı Kutucuk Tıklandı";
    this.selectedButtonIndex = index;
  }

  changeClass(index: any) {
    if (this.selectedButtonIndex === index) {
      return 'selected'; // Seçilen düğme için bir sınıf ekleyin
    } else {
      return ''; // Seçilmemiş düğme için boş bir dize döndürün
    }
  }

}
