import { Component, Input, OnInit, computed, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-test4',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './test4.component.html',
  styleUrl: './test4.component.scss',
  host: { ngSkipHydration: 'true' } // Hydrationun bu componentte kullanılmamasını saglar
})

export class Test4component implements OnInit {


  // Bir değişkeni Signal yaptığın zaman Angular değişkenin değerini Otomatik Değiştirmiyor. Biz söylediğimizde değiştiriyor.
  count = signal(0);
  count2 = 0;
  results = signal<string[]>([]);
  results2: string[] = [];
  @Input() id: string | null = null;
  @Input() name: string | null = null;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      this.name = params['name'];
    });
  }

  //computed (countplus değişkenini count değişkenine bağlıyoruz)
  countPlus = computed(() => this.count() + 1);

  //update,set yapabilirsin
  increment() {
    this.count2++;
    this.count.update((oldvalue) => oldvalue + 1);
    this.count.set(this.count() + 1);
    this.results2.push("increment");
    this.results.update((oldvalue) => [...oldvalue, 'increment'])
  }

  //update,set yapabilirsin
  decrement() {
    this.count2--;
    this.count.update((oldvalue) => oldvalue - 1);
    this.count.set(this.count() - 1);
    this.results2.push("decrement");
    this.results.update((oldvalue) => [...oldvalue, 'decrement'])
  }

  clear() {
    this.count.set(0);
    this.count2 = 0;
    this.results.set([]);
    this.results2 = [];
  }

}
