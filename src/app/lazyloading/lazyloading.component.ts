import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-lazyloading',
  templateUrl: './lazyloading.component.html',
  styleUrl: './lazyloading.component.scss',
  imports: [CommonModule],
  standalone: true
})

export class LazyloadingComponent {

}
