import { Component, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';
import { decrement, increment, reset } from '../counter.action';
import { CounterState } from '../counter.state';


@Component({
  selector: 'app-credit',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './credit.component.html',
  styleUrl: './credit.component.scss'
})

export class CreditComponent {

  krediTutari: number = 0;
  taksitler: number[] = [3, 6, 12, 24];
  taksitSayisi: number = 3;
  result: string = "";
  odemePlani: { taksitTutari: number, kalanGeriOdeme: number }[] = [];
  count$: Observable<number>


  constructor(private store: Store<{ countState: CounterState }>) {
    this.count$ = store.select(state => state.countState.count);
  }

  hesapla() {
    const taksitTutari: number = (this.krediTutari / this.taksitSayisi) * 1.29;
    let toplamGeriOdeme: number = taksitTutari * this.taksitSayisi;
    this.result = `Taksit Tutarı: ${taksitTutari} - Taksit Sayısı: ${this.taksitSayisi}-Toplam Geri Ödeme:${toplamGeriOdeme}`

    this.odemePlani = [];

    for (let index = 0; index < this.taksitSayisi; index++) {
      toplamGeriOdeme -= taksitTutari;
      const element = {
        taksitTutari: taksitTutari,
        kalanGeriOdeme: toplamGeriOdeme
      }
      this.odemePlani.push(element);
    }
  }

  increment() {
    this.store.dispatch(increment())
  }

  decrement() {
    this.store.dispatch(decrement())
  }

  reset() {
    this.store.dispatch(reset())
  }

}


