import { Component } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.scss'
})
export class SidebarComponent {

  constructor(
    private router: Router,
    private _toastr: ToastrService,
   private authService: AuthService) {
  }


  logout() {

    Swal.fire({
      text: "Mesaj Kısmı",
      title: "Başlık Kısmı",
      confirmButtonText: "Onayla",
      showConfirmButton: true,
      showCancelButton: true,
      cancelButtonText: "Vazgeç",
      cancelButtonColor: "Green"
    }).then(res => {
      if (res.isConfirmed) {
        this._toastr.warning("Çıkış Yapıldı", "Warning");
        //localStorage.clear;
        console.log("local Storage temizlendi");
        this.authService.logout();
        this.router.navigateByUrl("/login");
      } else {
        this._toastr.success("Çıkış Yapılmadı", "Success");
      }
    })


  }

}
