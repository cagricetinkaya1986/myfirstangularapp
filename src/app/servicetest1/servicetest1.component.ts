import { Component } from '@angular/core';
import { Servicetest2Component } from '../servicetest2/servicetest2.component';
import { FormsModule } from '@angular/forms';
import { ExampleService } from '../services/example.service';

@Component({
  selector: 'app-servicetest1',
  standalone: true,
  imports: [Servicetest2Component, FormsModule],
  templateUrl: './servicetest1.component.html',
  styleUrl: './servicetest1.component.scss'
})
export class Servicetest1Component {
  constructor(
    public services: ExampleService
  ) {
  }
}
