import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { SocialAuthService } from '@abacritt/angularx-social-login';
import { GoogleLoginProvider } from '@abacritt/angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedInSubject = new BehaviorSubject<boolean>(false);
  loggedIn$ = this.loggedInSubject.asObservable();

  constructor(
    private router: Router,
private socialAuthService: SocialAuthService,

  ) {
    const token = localStorage.getItem('token');
    this.loggedInSubject.next(!!token); // Sayfa yenilendiğinde kullanıcı durumunu kontrol et
  }

  isAuthenticated() {

    const token: string | null = localStorage.getItem("token");

    if (!token) {
      this.router.navigateByUrl("/login");
      return false;
    }

    return true;
  }

  login() {
    localStorage.setItem('token', 'ccet'); // Örnek token
    this.loggedInSubject.next(true);
  }

  logout() {

    this.socialAuthService.signOut();

    localStorage.removeItem('token');

    this.loggedInSubject.next(false);
  }

  isLoggedIn(): boolean {
    return this.loggedInSubject.value;
  }
}
