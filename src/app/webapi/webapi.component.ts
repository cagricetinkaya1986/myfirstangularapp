
import { Component } from '@angular/core';
import { UserModel } from '../models/user.model';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { WebapiService } from '../services/webapi.service';
import { Model } from '../models/model';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import {NgxPaginationModule} from 'ngx-pagination';

@Component({
  selector: 'app-webapi',
  standalone: true,
  imports: [FormsModule, CommonModule, NgxSpinnerModule,NgxPaginationModule],
  templateUrl: './webapi.component.html',
  styleUrl: './webapi.component.scss'
})
export class WebapiComponent {

  users: UserModel[] = [{
    id: '',
    name: '',
    surname: '',
    title: '',
    userId: '',
    completed: null
  }];

  p:number=1;

  usersNew: UserModel[] = [];

  model: Model;


  constructor(private _webapiservice: WebapiService, private _spinner: NgxSpinnerService, private _toastr: ToastrService) {

  }


  getApiGeneric() {
    this._webapiservice.getGenericApi((res) => {
      this.users = res;
      console.log(res);
    })
  }


  getApi1() {
    // Birinci dönüş yöntemi yakalama yöntemi Success donuyor fakat error donmuyor o yuzden timeout kondu
    this._spinner.show();

    // 5 saniye sonra spinner'ı kapatma işlemi
    const timeoutId = setTimeout(() => {
      this._spinner.hide();
      this._toastr.error("Servis Hatası", "Error");
    }, 5000);

    this._webapiservice.get1((data, success) => {

      // Zamanlayıcıyı temizle
      clearTimeout(timeoutId);

      if (success) {
        this.users = data;
        console.log(data);
        this._spinner.hide();
        this._toastr.success("Servise Ulasıldı", "Success");
      }

    })
  }

  getApi2() {
    this._webapiservice.get2((res) => {
      this.users = res;
      console.log(res);
    })
  }


  postApiGeneric() {

    this.model = {
      id: "1",
      userId: "99",
      title: "cemilim",
      completed: true
    }

    this._webapiservice.postGenericApi(this.model, (res) => {
      this.users = [res];
      console.log(res);
    })

  }

  postApi() {


    this.users[0].id = "1";
    this.users[0].userId = "99";
    this.users[0].title = "deneme Cemil";
    this.users[0].completed = false;



    // Birinci post yöntemi
    /* this._webapiservice.post(this.users[0]).subscribe({
       next: (res: UserModel) => {
         this.users = [res];
         console.log(res);
       }, error: (err: HttpErrorResponse) => {
         console.log(err);
       },
     })*/

    this._webapiservice.post(this.users[0], (res) => {
      this.users = [res];
      console.log(res);
    });
  };

  getApiInterceptor() {
    this._webapiservice.getGenericWithErrorPipe((res) => {
      console.log(res);
    })
  }


}


