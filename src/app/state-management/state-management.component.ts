import { Component, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-state-management',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './state-management.component.html',
  styleUrl: './state-management.component.scss'
})
export class StateManagementComponent {
//@Input ile dısardan alacagımızı belirttik, @Input({required=true}) seklinde tanımlarsak ana komponentte child komponenti çağırdıgımız yerde tanımlanması zorunlu olur
@Input() name:string="";

@Output() changeNameEvent = new EventEmitter<string>

@Input() todoss:string[] = [];

changeName(){
  this.changeNameEvent.emit(this.name);
}

}
