import { CanDeactivateFn } from '@angular/router';
import { TestComponent } from './test/test.component';

export const exitGuard: CanDeactivateFn<TestComponent> = (component, currentRoute, currentState, nextState) => {
  var result = confirm("Test Componentten Çıkmak İstiyor Musunuz!");
  return result;
};
