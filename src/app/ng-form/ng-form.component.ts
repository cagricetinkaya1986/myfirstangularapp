import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-ng-form',
  standalone: true,
  imports: [FormsModule, DatePipe, CommonModule],
  templateUrl: './ng-form.component.html',
  styleUrl: './ng-form.component.scss'
})

export class NgFormComponent {

  addModel = new Employee();
  employees: Employee[] = [];
  updateModel = new Employee();
  index: number = 0;
  isUpdateFormActive: boolean = false;

  constructor(
    private _date: DatePipe
  ) {
    this.addModel.startingDate = this._date.transform(new Date(), 'yyyy-MM-dd')
    this.updateModel.startingDate = this._date.transform(new Date(), 'yyyy-MM-dd')
  }


  save(form: NgForm) {
    if (form.valid) {
      console.log(form.value);
      this.employees.push(this.addModel);
      this.addModel = new Employee();
      this.addModel.startingDate = this._date.transform(new Date(), 'yyyy-MM-dd')
    }
  }

  get(model: Employee, index: number) {
    this.index = index;
    //bu sekilde kullanırsan referans vermis olursun tekrar guncelle dediğinde ekranda aşşağı taraf da kaydetmeden değişir kendini hemen ekranda günceller
    //this.updateModel = model;
    this.updateModel = {...model};
    this.isUpdateFormActive = true;
  }

  cancel() {
    this.isUpdateFormActive = false;
  }

  update(form: NgForm) {
    if (form.valid) {
      this.employees[this.index] = this.updateModel;
       this.isUpdateFormActive = false;
    }
  }

}


class Employee {
  name: string = "";
  profession: string = "";
  startingDate: string = "";
}
