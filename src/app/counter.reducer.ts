import { Action, createReducer, on } from "@ngrx/store";
import { decrement, increment, reset } from "./counter.action";
import { CounterState, initialState } from './counter.state';


// Recucer da CounterAction.ts dosyasında tanımladıgımız eylemeri uyguluyoruz


const _counterReducer = createReducer(
  initialState,
  on(increment, state => ({ ...state, count: state.count + 1 })),
  on(decrement, state => ({ ...state, count: state.count - 1 })),
  on(reset, state => ({ ...state, count: 0 }))
);

export function counterReducer(state: CounterState, action: Action<string>) {
  return _counterReducer(state, action);
}
