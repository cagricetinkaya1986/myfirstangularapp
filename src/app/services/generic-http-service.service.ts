import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ErrorService } from './error.service';

@Injectable({
  providedIn: 'root'
})
export class GenericHttpServiceService {

  apiUrl: string = "https://jsonplaceholder.typicode.com/"

  constructor(private _http: HttpClient, private _err: ErrorService) { }

  getGeneric(api: string, callBack: (res: any) => void) {

    let headers = {
      headers: {
        "authorization": localStorage.getItem("token")
      }
    }

    this._http.get(this.apiUrl + api, headers).subscribe({
      next: (res) => {
        callBack(res);
      }, error: (err: HttpErrorResponse) => {
        this._err.errorHandler(err);
      },
    })
  }

  postGeneric(api: string, model: any, callBack: (res: any) => void) {

    let headers = {
      headers: {
        "authorization": localStorage.getItem("token")
      }
    }

    this._http.post(this.apiUrl + api, model, headers).subscribe({
      next: (res) => {
        callBack(res);
      }, error: (err: HttpErrorResponse) => {
        this._err.errorHandler(err);
      },
    }

    )
  }

  getGenericWithErrorPipe(api: string, callBack: (res: any) => void) {

    let headers = {
      headers: {
        "authorizationError": localStorage.getItem("token")
      }
    }

    this.apiUrl = "hata almak için değiştirildi.";

     // error yazmadık direk test.interceptor altından error pipe ile hataları yakalayıp yönetebiliyoruz
    this._http.get(this.apiUrl + api, headers).subscribe({
      next: (res) => {
        callBack(res);
      }

    })
  }

}
