import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { catchError, of } from 'rxjs';

export const testInterceptor: HttpInterceptorFn = (req, next) => {

  // Burada isteği değiştirebilir veya loglayabilirsiniz
  let token = localStorage.getItem("token");
  console.log('Interceptor çalıştı:', req);
  // İsteğe Authorization başlığını ekleyin
  const modifiedReq = req.clone({
    setHeaders: {
      Authentication: `Bearer ${token}`
    }
  });

  // Değiştirilmiş isteği bir sonraki aşamaya iletttik pipe ile hata donerse yakalayıp mesaj olusturuyoruz boylece her requestte hata yazmaya gerek yok
  return next(modifiedReq).pipe(catchError((err: HttpErrorResponse) => {
    //errorhandler
    console.log(err);

    if (err.status == 404) {
      console.log("Error Handler Api Adresine Ulaşılamıyor");
    } else if (err.status == 200) {
      console.log("Error Handler 200");
    } else {
      console.log(err.message);
      console.log("cemilimmmmmm Hataaaa");
    }

    return of();

  }));
};
