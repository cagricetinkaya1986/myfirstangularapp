import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { UserModel } from '../models/user.model';
import { ErrorService } from './error.service';
import { GenericHttpServiceService } from './generic-http-service.service';


@Injectable({
  providedIn: 'root'
})

export class WebapiService {

  users: UserModel[] = [];

  constructor(private _http: HttpClient, private _error: ErrorService, private _genericService: GenericHttpServiceService) { }


  getGenericApi(callback: (res: any) => void) {
    this._genericService.getGeneric("todos", res => callback(res));
  }

  postGenericApi(model: any, callBack: (res: any) => void) {
    this._genericService.postGeneric("todos", model, res => callBack(res))
  }


  get1(callback: (res: UserModel[], success: boolean) => void) {

    let headers = {
      headers: {
        "authorization": "deger1"
      }
    }

    this._http.get<UserModel[]>("https://jsonplaceholder.typicode.com/todos", headers).subscribe(
      (data) => {
        // Başarı ile veri geldiğinde callback çağır
        callback(data, true);
      },
      (error) => {
        // Hata durumunda callback ile false döndür
        console.error('Error fetching data:', error);
        callback([], false);
      })

  }

  get2(callback: (res: UserModel[]) => void) {

    let headers = {
      headers: {
        "authorization": "degerrrrr"
      }
    }

    this._http.get<UserModel[]>("https://jsonplaceholder.typicode.com/todos", headers).subscribe({
      next: (res: UserModel[]) => {
        this.users = res;
        callback(this.users);
        console.log(res);
      }, error: (err: HttpErrorResponse) => {
        this._error.errorHandler(err); // Hata yöneticisini çağırın
      },
    })

  }

  post(model: UserModel, callBack: (res: UserModel) => void) {

    // Birinci post yöntemi post içinde callBack:(res:UserModel)=>void burası yok
    //return this._http.post<UserModel>("https://jsonplaceholder.typicode.com/todos", model);

    this._http.post<UserModel>("https://jsonplaceholder.typicode.com/todos", model).subscribe({
      next: (res) => {
        callBack(res)
        console.log(res);
      }, error: (err: HttpErrorResponse) => {
        this._error.errorHandler(err)
      },
    });

  }


  getGenericWithErrorPipe(callBack: (res: any) => void) {
    this._genericService.getGenericWithErrorPipe("todos", res => callBack(res));
  }

}


