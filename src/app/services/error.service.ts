import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(private _toastr: ToastrService) { }

  errorHandler(err: HttpErrorResponse) {
    if (err.status == 404) {
      console.log("Api Adresine Ulaşılamıyor");
      this._toastr.error("Api Adresine Ulaşılamıyor", "Error"); // Hata mesajı gösterin
    } else if (err.status == 200) {
      console.log("Error Handler 200");
        this._toastr.error("Serviste Hata Oluştu", "Error"); // Hata mesajı gösterin
    } else {
      console.log(err.message);
      console.log("cemilimmmmmm Hataaaa");
        this._toastr.error("Serviste Hata Oluştu", "Error"); // Hata mesajı gösterin
    }
  }

}
