export class UserModel {
  id:string;
  name: string;
  surname: string;
  title:string;
  userId:string;
  completed:boolean;
}
