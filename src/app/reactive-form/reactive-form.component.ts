import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


@Component({
  selector: 'app-reactive-form',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule, FormsModule],
  templateUrl: './reactive-form.component.html',
  styleUrl: './reactive-form.component.scss',
  providers: [DatePipe]
})


// OnInit yasam dongusunu import ettik
export class ReactiveFormComponent implements OnInit {

  addForm: FormGroup = new FormGroup({});
  updateForm: FormGroup = new FormGroup({});
  employees: Employee[] = [];
  isUpdateFormActive: boolean = false;
  updateIndex: number = 0;

  constructor(private _date: DatePipe) {

  }

  // ngOnInit Methodu ilk Önce Olusuyor
  ngOnInit(): void {
    this.createAddForm();
  }

  createAddForm() {
    this.addForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(3)]),
      //date pipe ekledigimiz için transform yapabiliyoruz
      startingDate: new FormControl(this._date.transform(new Date(), 'yyyy-MM-dd')),
      //startingDate: new FormControl(this.formatDate(new Date())),
      profession: new FormControl(Validators.required)
    })
  }

  createUpdateForm() {
    this.updateForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(3)]),
      //date pipe ekledigimiz için transform yapabiliyoruz
      startingDate: new FormControl(this._date.transform(new Date(), 'yyyy-MM-dd')),
      //startingDate: new FormControl(this.formatDate(new Date())),
      profession: new FormControl(Validators.required)
    })
  }


  get(model: Employee, index: number) {
    this.createUpdateForm();
    this.updateForm.controls["profession"].setValue(model.profession);
    this.updateForm.controls["name"].setValue(model.name);
    this.updateForm.controls["startingDate"].setValue(model.startingDate);
    this.isUpdateFormActive = true;
    this.updateIndex = index;
  }

  cancel() {
    this.isUpdateFormActive = false;
  }

  update() {
    if (this.updateForm.valid) {
      this.employees[this.updateIndex] = this.updateForm.value;
      this.cancel();
    }
  }

  // Tarihi 'yyyy-MM-dd' formatına dönüştüren bir yardımcı fonksiyon
  formatDate(date: Date): string {
    const d = new Date(date);
    const month = ('0' + (d.getMonth() + 1)).slice(-2);
    const day = ('0' + d.getDate()).slice(-2);
    const year = d.getFullYear();
    return `${year}-${month}-${day}`;
  }

  Kaydet() {
    if (this.addForm.valid) {
      this.employees.push(this.addForm.value);

      //this.addForm.reset();
      //this.addForm.controls["startingDate"].setValue(this._date.transform(new Date(), 'yyyy-MM-dd'))

      //Formu Sıfırlar yukardaki kapattıgımı kullanmaya gerek yok
      this.createAddForm();
    }
  }

}

class Employee {
  name: string = "";
  profession: string = "";
  startingDate: string = "";
}
