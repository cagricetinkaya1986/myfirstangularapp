import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TodoPipe } from './todo.pipe';
import { NamePipe } from './name.pipe';
import { TrCurrencyPipe } from 'tr-currency';

@Component({
  selector: 'app-todo',
  standalone: true,
  imports: [FormsModule, CommonModule, TodoPipe, NamePipe, TrCurrencyPipe],
  templateUrl: './todo.component.html',
  styleUrl: './todo.component.scss'
})

export class TodoComponent {
  work: string = "";
  works: string[] = ["Deneme1", "Deneme2", "Deneme3"];
  updateWork: string = "";
  updateFormActive: boolean = false;
  indeks: number = 0;
  search: string = "";
  name:string="Cemil";
  surname:string="Cetinkaya";
  date:Date=new Date();
  salary:number=17002.25;

  save() {
    this.works.push(this.work);
    this.work = "";
  }

  remove(index: number) {
    let result: boolean = confirm("Kaydı Silmek İstiyor Musunuz?");
    if (result == true) {
      this.works.splice(index, 1)
    }
  }

  update() {
    this.works[this.indeks] = this.updateWork;
    this.cancel();
  }

  get(work: string, indeks: number) {
    this.updateWork = work;
    this.updateFormActive = true;
    this.indeks = indeks;
  }

  cancel() {
    this.updateFormActive = false;
  }

  changeInputClass() {
    if (this.work.length < 3)
      return "is-invalid"

    return "is-valid"
  }


}
