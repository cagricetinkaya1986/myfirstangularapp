import { Component,NgModule } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterModule, FormsModule, ReactiveFormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})

export class AppComponent {

  title = 'my-first-app';
  name:string="cemil";
  //string,number,boolean,date,undefined,any,object
  age:number|undefined;
  ageCemil:number=31;
  surname:string="cetinkaya";

 constructor(){
  console.log(this.ageCemil);
 }


 metot(){
  alert(this.name + " Angular Öğreniyor!!!")
 }

}


