import { APP_INITIALIZER, Provider } from '@angular/core';

// Bu fonksiyon, uygulama başlatıldığında çalıştırılır
export function clearTokenOnRestart(): () => void {
  return () => {
    const isloged = localStorage.getItem('token');

    if (!isloged) {
      // Token'ı localStorage'dan temizleyin
      localStorage.removeItem('authToken');
      localStorage.removeItem('token');
      localStorage.removeItem('tokenExpiration'); // Eğer token süresi eklediyseniz bunu da kaldırabilirsiniz
      console.log('Token temizlendi');
      // Gerekirse diğer oturum temizleme işlemleri burada yapılabilir
    }
  };
}



// APP_INITIALIZER sağlayıcısı
export const appInitProvider: Provider = {
  provide: APP_INITIALIZER,
  useFactory: clearTokenOnRestart,
  multi: true
};
