import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CommentComponent } from '../comment/comment.component';
import { UsersComponent } from '../users/users.component';
import { UserModel } from '../models/user.model';
import { Store } from '@ngrx/store';
import { addUser } from '../users/users.action';

@Component({
  selector: 'app-test3',
  standalone: true,
  imports: [CommonModule, FormsModule, CommentComponent, UsersComponent],
  templateUrl: './test3.component.html',
  styleUrl: './test3.component.scss'
})

export class Test3Component {
  durum: string = "";
  firstWord: boolean = false;
  isActive: boolean = true;
  meyve: string = "";
  meyveler: string[] = ["elma", "armut", "muz"];
  role: string = "admin";
  showComment: boolean = false;
  todos: TodoModel[] = [
    { work: "Example 1", isCompleted: true },
    { work: "Example 2", isCompleted: true },
    { work: "Example 3", isCompleted: false }
  ]

  user: UserModel = new UserModel();

  constructor(
    private store: Store<{ users: UserModel[] }>
  ) {
    this.forEachExample();
    this.forExample();
    this.changeSatatus();
  }

  changeStatus() {
    this.isActive = !this.isActive;
  }

  listeEkle() {
    this.meyveler.push(this.meyve);
    this.meyve = "";
  }

  forEachExample() {
    this.todos.forEach(element => {
      console.log(element.work);
      console.log(element.isCompleted);
    });
  }

  forExample() {
    for (let index = 0; index < this.todos.length; index++) {
      console.log(this.todos[index].work)
    }

    // Of da Asenkron kullanım mevcut
    for (let data of this.todos) {
      console.log(data.work);
    }

    for (let index in this.todos) {
      console.log(index);
    }

  }

  changeSatatus() {
    this.durum = this.firstWord == true ? 'true' : 'false';
    console.log(this.durum);
  }

  add() {
    this.store.dispatch(addUser({ user: this.user }));
    this.user = new UserModel();
  }

}

// Modeller Projelerde Models Altına konulur
export class TodoModel {
  work: string = "";
  isCompleted: boolean = false;
}
