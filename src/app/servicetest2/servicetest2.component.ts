import { Component } from '@angular/core';
import { ExampleService } from '../services/example.service';

@Component({
  selector: 'app-servicetest2',
  standalone: true,
  imports: [],
  templateUrl: './servicetest2.component.html',
  styleUrl: './servicetest2.component.scss'
})
export class Servicetest2Component {
 constructor(
    public services: ExampleService
  ) {
  }
}
