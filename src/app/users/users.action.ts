import { createAction,props } from "@ngrx/store";
import { UserModel } from "../models/user.model";

export const addUser = createAction('[Users] addUser',
 props<{user:UserModel}>());

