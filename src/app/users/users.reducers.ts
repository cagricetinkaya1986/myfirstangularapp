import { Action, createReducer, on } from "@ngrx/store";
import { addUser } from "./users.action";
import { UserModel } from "../models/user.model";

export const initialState: UserModel[] = [];

export const usersReducer = createReducer(
  initialState,
  on(addUser, (state, { user }) => {
  //readOnly olan User Objectinin referansını olusturup işledik değiştirebildik
  const newUser = {...user, name:"Bay " + user.name}
  return [...state, newUser]; }))
