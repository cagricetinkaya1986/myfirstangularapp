import { Component, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RenklendirDirective } from './renklendir.directive';
import { ValidDirective } from './valid.directive';
import { StructuralDirective } from './structural.directive';
import { Router } from '@angular/router';
import {StateManagementComponent} from '../state-management/state-management.component';



@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})

export class TestComponent {

   constructor(
    private router:Router
   ){
   }

  adress: string = "Ankara";
  result:boolean = false;
  firstValid:boolean = false;
  secondValid :boolean=false;
  pClassName:string="blueClass";
  name:string="StateManagemenent Name Cemil";
  work:string="";
  todoss:string[]=[];

  getAdress() {
    console.log(this.adress);
  }

  //parametreli Routes parametre cemil gidiyor
  goPage() {
    this.router.navigateByUrl("test2/cemillll");
  }


changeName(event:string){
  this.name = event;
}

  add() {
    this.todoss.push(this.work);
    this.work = "";
  }

}

@NgModule({
  declarations: [
    TestComponent,
    RenklendirDirective,
    ValidDirective,
    StructuralDirective
  ],
  imports: [
    FormsModule, // FormsModule'ü import et
    StateManagementComponent,
    CommonModule
],
  exports: [
    TestComponent
  ]
})

export class TestModule { }
