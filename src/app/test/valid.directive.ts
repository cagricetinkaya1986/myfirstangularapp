import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from "@angular/core";

@Directive({
  selector: "[appValid]",
})

export class ValidDirective {

  ngOnInit() {

  }


  @Input() appValid:boolean=false;
  @Output() validChange = new EventEmitter<boolean>();


  constructor(
    private _el: ElementRef
  ) {
    if (this._el.nativeElement) {
      // DOM'a erişim ve işlemler burada gerçekleştirilir
      this._el.nativeElement.className = "form-control";
    }
  }

// Tekli Validate
/*
  @HostListener("keyup") keyup(){
    if(this.appValid){
      this._el.nativeElement.className="form-control is-valid";
      console.log(this.appValid)
    }else{
      this._el.nativeElement.className="form-control is-invalid"
      console.log(this.appValid)
    }
  }

  @HostListener("keypress") keypress(){
    if(this.appValid){
      this._el.nativeElement.className="form-control is-valid";
      console.log(this.appValid)
    }else{
      this._el.nativeElement.className="form-control is-invalid"
      console.log(this.appValid)
    }
  }
*/

@HostListener('keyup') onKeyUp() {

  console.log(this._el.nativeElement.value);

  if (this._el.nativeElement.getAttribute('id') == "directive") {
    const isValid = this.validateValue(this._el.nativeElement.value);
    if (isValid){
      this._el.nativeElement.className="form-control is-valid";
    }else{
      this._el.nativeElement.className="form-control is-invalid"
    }
    this.validChange.emit(isValid);
  } else {
    const isSecondValid = this.validateSecondValue(this._el.nativeElement.value);
    if (isSecondValid){
      this._el.nativeElement.className="form-control is-valid";
    }else{
      this._el.nativeElement.className="form-control is-invalid"
    }
    this.validChange.emit(isSecondValid);
  }

}


validateValue(value: any): boolean {
  const length = value.length;
  return value !== null && value !== undefined && value !== '' && length == 1;
}


validateSecondValue(value: any): boolean {
  const length = value.length;
  return value !== null && value !== undefined && value !== '' && length > 3;
}

}
