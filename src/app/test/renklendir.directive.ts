import { Directive, ElementRef, HostListener, Input } from "@angular/core";

@Directive({
  selector: "[appRenklendir]"
})

export class RenklendirDirective {

  @Input() appRenklendir:string="";
  @Input() test:string="";


  constructor(
    private _el: ElementRef
  ) {
    this._el.nativeElement.style.backgroundColor="Yellow";
    this._el.nativeElement.className="form-control";
  }

  @HostListener("mouseenter") metot(){
    this._el.nativeElement.style.backgroundColor="red";
    console.log('aaa'+this.test);
    console.log('bbb'+this.appRenklendir);
    this._el.nativeElement.innerHTML = this.test;
  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave(event: MouseEvent) {
    // Fare öğeden çıktığında burası çalışacak
    //this._el.nativeElement.style.backgroundColor="yellow";
    this._el.nativeElement.style.backgroundColor=this.appRenklendir;
  }


}
