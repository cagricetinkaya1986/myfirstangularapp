import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appStructural]'
})
export class StructuralDirective {


  @Input() set appStructural(result:boolean){
    if (result){
         this.viewContainer.createEmbeddedView(this.templateRef);
    }else{
         this.viewContainer.clear();
    }
  }

  constructor(
    private templateRef:TemplateRef<any>,
    private viewContainer:ViewContainerRef
  ) { }

}
