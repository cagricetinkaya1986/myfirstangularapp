import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { importProvidersFrom } from '@angular/core';
import { provideStore } from '@ngrx/store';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { counterReducer } from './app/counter.reducer';
import { usersReducer } from './app/users/users.reducers';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { testInterceptor } from './app/services/test.interceptor';
import { appInitProvider } from './app/app-init';
import { ReactiveFormsModule } from '@angular/forms';
import { routes } from './app/app.routes';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { BrowserAnimationsModule, provideAnimations } from '@angular/platform-browser/animations';
import { provideToastr } from 'ngx-toastr';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from '@abacritt/angularx-social-login';

bootstrapApplication(AppComponent, {
  providers: [
    provideRouter(routes),
    appConfig.providers,
    importProvidersFrom(ReactiveFormsModule),
    provideStore({ countState: counterReducer }),
    provideStore({ users: usersReducer }),
    provideStoreDevtools({ maxAge: 25 }),
    provideHttpClient(withInterceptors([testInterceptor])),
    appInitProvider,
    provideAnimations(), // required animations providers
    provideToastr({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
      progressBar: true
    }), // Toastr providers
    SweetAlert2Module,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    SocialLoginModule,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        lang: 'en',
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '565020809504-0k4mef49oqtgtrti2lskn9f8u6andi4e.apps.googleusercontent.com'
            )
          }
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
}).catch(err => console.error(err));
